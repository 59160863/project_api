const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
let contacts = [
 {
    name : 'John',
    phoneNumber : '011-1111111'
 },

 {
    name : 'Supatsorn ',
    phoneNumber : '083-6222013'
 },

 {
    name : 'Thaweechai',
    phoneNumber : '098-4230099'
 },

 {
    name : 'Suthirat',
    phoneNumber : '094-5611623'
}
]

app.use(bodyParser.json())
app.use(cors())

app.get('/contacts', (req,res) => {
    res.json(contacts)
})

app.post('/contacts', (req,res) => {
    let newContact = req.body
    contacts.push(newContact)
    res.status(201).json(newContact)
})

app.listen(3000, () => {
    console.log('API Server started at port 3000')
})
